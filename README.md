# Regall2Vec

This repository is intended as a quick introduction to 1) software engineering basics, and 2) Word2Vec / Doc2Vec.

## Session Overview

[Session 1](#session-1)
1. Python 3 and Conda
2. IDE (Intellij IDEA)
3. Git
4. Python data structures / Introduction to Python docs

[Session 2](#session-2)
1. Word2Vec
2. Doc2Vec
3. Introduction to GloVe, FastText

[Session 3](#session-3)
1. Python libraries
    1. pathlib
    2. gensim
2. Revision

## Session 1

### Downloads
+ [Anaconda](https://www.anaconda.com/download/)
+ [Intellij IDEA Ultimate](https://www.jetbrains.com/student/)
+ [Git](https://git-scm.com/downloads)

### Python 3 and Conda
Python is a programming language.
By itself, Python not very useful. But, other developers create _libraries / packages_ that extends Python.

Conda is a _package manager_ for Python, and is installed alongside the Anaconda distribution. 

Projects may have conflicting requirements (i.e. Python 2 instead of Python 3). Anaconda allows users to create multiple Python _environments_ on the same machine.

### IDEs and Intellij IDEA
Python code is essentially a text file with the file extension `.py`. They can be edited with any text editor, e.g. Notepad.

Integrated Development Environments (IDEs) allow developers to work on projects easily. They usually allow many files to be open at once, and some have built-in syntax highlighting.
<details>
<summary>Syntax Highlighting</summary>

Plain text:
```
print('Hello world!')
for i in range(10):
    print(i)
```


Python syntax highlighting:
```python
print('Hello world!')
for i in range(10):
    print(i)
```

</details>


Intellij IDEA is a well known Java IDE, but the Ultimate edition supports Python code as well. 

### Git
Git is a version control system commonly used by developers to manage code. Each project usually belongs to a _repository_, and developers _commit_ changes to this repository.

Not all files are tracked by git! To add a file to a git project, use `git add <file>`, replacing `<file>` with the file you want to add.

In the Project pane of Intellij IDEA, you can see the filenames in different colors corresponding to their git status.
A brief summary of the statuses is given in the table below.

| Color | Tracked by Git? | Changes to be committed? |                                              Comments                                             |                                           Next steps                                           |
|:-----:|:---------------:|:------------------------:|:-------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------:|
|  Red  |        No       |             -            |                              This file is _not_ being tracked by git!                             |  If this file is required to run the project, add to git via `git add <file>`. Else, ignore it |
| Green |        No       |            Yes           |             This is a newly added file; changes to it has _not_ been committed to git.            |                                    Commit and push upstream.                                   |
|  Blue |       Yes       |            Yes           |                          Changes to this file has _not_ been committed!                           |                                    Commit and push upstream.                                   |
| White |       Yes       |            No            | Changes to this file has been committed locally; however, it _may not_ have been pushed upstream! |                                                -                                               |

A typical Git workflow is as follows:
1. Create/clone a git repository
    * Creating a new repo: `git init`
    * Cloning an existing repo: `git clone <url>`
2. Make changes to the project
3. Commit your changes __(local!)__
    * `git commit -m '<commit msg>'`
4. Push changes to the parent repository
    * (Optional) Make sure your repository is up to date: `git pull`
    * `git push`

>__\<WARNING\>__ Make sure that repository is up to date before pushing! Git conflicts occur when there are conflicting changes in the commits being pushed, and resolving them is tricky and out of scope of this session. __\</WARNING\>__

P.S. Screwing up in git is common / expected. Refer to [this](https://ohshitgit.com/) for some helpful scripts to fix your git repository.

### Data Structures
1. [Restricted keywords](https://docs.python.org/3/reference/lexical_analysis.html?highlight=reserved#keywords)
2. [Indentation](https://docs.python.org/3/reference/lexical_analysis.html?highlight=reserved#indentation)
3. [Operators](https://docs.python.org/3/reference/lexical_analysis.html?highlight=reserved#operators)
4. [Variables](https://docs.python.org/3/tutorial/introduction.html#strings)
    + Strings
4. [Flow Control](https://docs.python.org/3/tutorial/controlflow.html)
    + `if`
    + `for`
    + `while`
    + `else`
    + `functions`
5. [Data structures](https://docs.python.org/3/tutorial/datastructures.html?highlight=list%20comprehension)
    + Lists
    + List Comprehension
    + Tuples
    + Sets
    + Dictionaries (key-value pairs)
    + Loops
    
## Session 2

### Word2Vec

Paper: [Distributed representations of Words and Phrases and their Compositionality](https://papers.nips.cc/paper/5021-distributed-representations-of-words-and-phrases-and-their-compositionality.pdf)

Code: [Word2Vec Project Page](https://code.google.com/archive/p/word2vec/) (Hint: pre-trained embeddings available here!)

![One-hot Embeddings](https://cdn-images-1.medium.com/max/800/1*zFzLwr9kMiuT5FH9TPwd-Q.png)

Word embeddings are representations of words in an numerical form. Simplest form of a word embedding is a one-hot vector.

![Word2Vec Models](https://skymind.ai/images/wiki/word2vec_diagrams.png)

One of the first models to be widely used was Word2Vec. There are two training modes, CBOW and Skip-gram. 
CBOW (Continuous Bag of Words) predicts the middle word based on the context words, while Skip-gram does the reverse.

![CBOW Model](https://cdn-images-1.medium.com/max/800/1*UdLFo8hgsX0a1NKKuf_n9Q.png)
![Skip-gram Model](https://cdn-images-1.medium.com/max/800/1*TbjQNQLuyEW-cgsofyDioQ.png)

What we are trying to learn is the weights for each 'operation', i.e. for the projection as well as the output.

![Word2Vec Illustration](https://www.distilled.net/uploads/word2vec_chart.jpg)

Thinking of words as vectors allow us to plot them, and some interesting relations can be observed.

![Word2Vec Illustration](https://skymind.ai/images/wiki/word2vec_translation.png)

These embeddings are also invariant to the language used in the training data.

### Doc2Vec

Paper: [Distributed Representations of Sentences and Documents](https://arxiv.org/pdf/1405.4053.pdf)

![Doc2Vec Illustration](https://i.stack.imgur.com/ofJqR.png)

Doc2Vec is an augmentation of Word2Vec, where paragraph embeddings are trained at the same time.
These paragraph embeddings can then be used to identify similarity between different paragraphs.

### Introduction to GloVe and FastText

The following sections are meant to give a brief overview of how word representation research has progressed since Word2Vec.

### GloVe

Paper: [GloVe: Global Vectors for Word Representation](http://www.aclweb.org/anthology/D14-1162)

Code: [GloVe Project Page](https://nlp.stanford.edu/projects/glove/)

![Word-Word Co-occurrence Probabilities](https://nlp.stanford.edu/projects/glove/images/table.png)

Co-occurrence of target word `ice` is higher to probe word `solid` than `gas`, but vice versa for target word `water`.

For target words `{ice, steam}`, the co-occurrence probability with noisy, non-discriminative probe words `{water, fashion}` is similar.
Hence, the ratio of the target words to these probe words (i.e. $`P(k|ice)/P(k|steam)`$) is approximately 1.

In contrast, the ratio of the target words to discriminative probe words are informative (see probe words `{solid, gas}`).

GloVe Loss Function: $`\textrm{dot}(w_i, \tilde{w_k}) + b_i + \tilde{b}_k = \log(X_{ik})`$, where $`X_{ik}`$ is the occurrence of the word $`k`$ in the context of the target word $`i`$.

### FastText

Paper: [Enriching Word Vectors with Subword Information](https://arxiv.org/pdf/1607.04606.pdf)

Code: [FastText Project Page](https://fasttext.cc/)

Word2Vec requires that all testing inputs appear in the training data, which is not always possible. 
([Zipf's Law](https://en.wikipedia.org/wiki/Zipf%27s_law): Frequency of words is inversely proportional to the word's frequency rank.)

In addition to learning the word embeddings, FastText also generates embeddings for substrings during training.
When unknown words occur in testing, assign it an embedding that is the combination of the embeddings of all its substrings.

![Subword Information](https://camo.qiitausercontent.com/27ae86df54cc53e4b51d3e1b7c3a270ec04b2e4d/68747470733a2f2f71696974612d696d6167652d73746f72652e73332e616d617a6f6e6177732e636f6d2f302f32353939302f36623133626561332d373633392d636462372d363735662d3661366562333133656536332e706e67)

### Useful Links

+ [History of word embeddings](http://hunterheidenreich.com/blog/intro-to-word-embeddings/)

## Session 3

<Work in progress>